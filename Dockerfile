FROM debian:jessie

# To avoid problems with Dialog and curses wizards
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
	apt-get install -qq aptitude && \
	apt-get install --no-install-recommends -qq \
		curl wget ca-certificates unzip libglib2.0-0 libfontconfig libx11-6 libxi6 \
		libxcursor1 libxss1 libxcomposite1 libxdamage1 libxtst6 libdbus-1-3 \
		libsnappy1 libgl1-mesa-glx libgl1-mesa-dri xdg-utils \
  apt-get clean


RUN curl http://dls.photoprintit.com/api/getClient/7884-fr_FR/hps/om_seo_goo_x_30574_x_180608KEGCzUH9LiLho_oNKntSYahhjUf/linux > /tmp/cewe.tgz && \
	tar xvf /tmp/cewe.tgz && \
	cp /bin/echo /bin/less && \
	rm EULA.txt && touch EULA.txt && \
  echo -e "\noui\noui\noui" | ./install.pl -i /opt

# Create a user
ENV USER cewe
ENV UID 1000
ENV GROUPS audio,video
ENV HOME /home/$USER
RUN useradd -m -d $HOME -u $UID -G $GROUPS $USER

USER $USER
WORKDIR $HOME

CMD [ "/opt/CEWE Photo" ]