# CEWE container

Photo album creation software (https://www.cewe.fr/). **Not an official image**.

Run this image with:

```sh
xhost +local:docker
docker run bagage/cewe \
           -e PULSE_SERVER=unix:$XDG_RUNTIME_DIR/pulse/native \
           -e DISPLAY=$DISPLAY \
           -v $PWD/cewe-data:/home/cewe \
           -v $XDG_RUNTIME_DIR/pulse:/run/user/1000/pulse \
           -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
           cewe
```  

Where:

- xhost is needed so that application can be opened in docker
- PULSE_SERVER and XDG_RUNTIME_DIR are needed for audio in CEWE
- DISPLAY and X11-unix are needed for displaying CEWE window
- `$PWD/CEWE-data` is the path on the host machine to folder where CEWE configuration / data will be persisted.